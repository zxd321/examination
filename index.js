// 获取是否移动端还是pc
var isPc = window.screen.width > 414 ? true : false;
if (!isPc) {
	document.documentElement.style.fontSize = document.body.clientWidth * 100 / 375 + "px";
}
bannerOneSwiperFun(isPc)
bannerTwoSwiperFun(isPc)
window.onresize = function() {
	var isPc = window.screen.width > 414 ? true : false;
	if (!isPc) {
		document.documentElement.style.fontSize = document.body.clientWidth * 100 / 375 + "px";
	}
}
// banner-one 轮播函数
function bannerOneSwiperFun(isPc) {
	let html = '';
	for (var i = 1; i < 9; i++) {
		html += `
		<div class="swiper-slide">
			<img src="${isPc? 'pc': 'mobile'}/congratulate/hot_${i}.jpg">
		</div>
		`;
	}
	$('#banner-one .swiper-wrapper').html(html);
	var bannerOneSwiper = new Swiper('#banner-one', {
		pagination: '.swiper-pagination',
		autoplay: 3000, //可选选项，自动滑动
		speed: 1000,
		loop: true,
		observer: true,
		paginationClickable: true,
		autoplayDisableOnInteraction: false
	})
}
// banner-two 轮播函数
function bannerTwoSwiperFun(isPc) {
	let html = '';
	for (var i = 1; i < 8; i++) {
		html +=
			`
		<div class="swiper-slide">
			<img src="${isPc? 'pc': 'mobile'}/congratulate/${i}.${isPc? 'png': 'jpg'}">
		</div>
		`;
	}
	$('#banner-three .swiper-wrapper').html(html);
	var bannerThreeSwiper = new Swiper('#banner-three', {
		autoplay: 4000, //可选选项，自动滑动
		loop: true,
		observer: true,
		slidesPerView: isPc ? 4 : 3,
		spaceBetween: 29,
		autoplayDisableOnInteraction: false
	});
}

var guangboBannerSwiper = new Swiper('#guangbo-banner', {
	direction: 'vertical',
	autoplay: 2000, //可选选项，自动滑动
	loop: true,
	autoplayDisableOnInteraction: false,
})
var guangboBannerSwiper = new Swiper('#guangbo-banner-1', {
	direction: 'vertical',
	autoplay: 2000, //可选选项，自动滑动
	loop: true,
	autoplayDisableOnInteraction: false,
})
var studentBannerSwiper = new Swiper('#student-banner', {
	autoplay: 4000, //可选选项，自动滑动
	loop: true,
	slidesPerView: isPc ? 3 : 'auto',
	centeredSlides: isPc ? false : true, //每屏，居中显示
	spaceBetween: isPc ? 75 : 0,
	simulateTouch: false,
	prevButton: '.student-banner-prev',
	nextButton: '.student-banner-next',
})
$('#student-banner').hover(function() {
	studentBannerSwiper.stopAutoplay();
}, function() {
	studentBannerSwiper.startAutoplay();
});

$('#student-banner .btn').click(function() {
	$(this).parent('.zheng').css('-webkit-transform', 'rotateY(-180deg)').siblings('.fan').css('-webkit-transform',
		'rotateY(-360deg)');
})
$('#student-banner .fan-btn:nth-child(1)').click(function() {
	$(this).parent().parent('.fan').css('-webkit-transform', 'rotateY(-180deg)').siblings('.zheng').css(
		'-webkit-transform',
		'rotateY(0deg)');
})
$('#student-banner .fan-btn:nth-child(2)').click(function() {
	xunwenClick();
	if(isPc) return;
	$('.company-info').css('margin-bottom', '.5rem')
})
var isProject = true;
var isClass = true;
var headerList = {
	name: '',
	project: isPc?'': '1',
	class_name: isPc?'': '1',
	phone: ''
}
var isProject8 = true;
var isClass8 = true;
var eightList = {
	name: '',
	project: isPc?'': '1',
	class_name: isPc?'': '1',
	phone: ''
}

// $('#pc-head-project').click(function() {
// 	isProject = !isProject;
// 	if (isProject) return $(this).children('.dropdown-menu').css('display', 'none');;
// 	$(this).children('.dropdown-menu').css('display', 'block');
// })
// $('#project-one').click(function() {
// 	isProject = !isProject;
// 	if (isProject) return $(this).children('.dropdown-menu').css('display', 'none');;
// 	$(this).children('.dropdown-menu').css('display', 'block');
// })
$('#pc-head-class').click(function() {
	isClass = !isClass;
	if (isClass) return $(this).children('.dropdown-menu').css('display', 'none');;
	$(this).children('.dropdown-menu').css('display', 'block');
})
$('#class-none').click(function() {
	isClass = !isClass;
	if (isClass) return $(this).children('.dropdown-menu').css('display', 'none');;
	$(this).children('.dropdown-menu').css('display', 'block');
})
// $('#project-two').click(function() {
// 	isProject8 = !isProject8;
// 	if (isProject8) return $(this).children('.dropdown-menu').css('display', 'none');;
// 	$(this).children('.dropdown-menu').css('display', 'block');
// })
// $('#pc-project-two').click(function() {
// 	isProject8 = !isProject8;
// 	if (isProject8) return $(this).children('.dropdown-menu').css('display', 'none');;
// 	$(this).children('.dropdown-menu').css('display', 'block');
// })
$('#class-two').click(function() {
	isClass8 = !isClass8;
	if (isClass8) return $(this).children('.dropdown-menu').css('display', 'none');;
	$(this).children('.dropdown-menu').css('display', 'block');
})
// 冲刺科目的下拉点击
$('#project-one li').click(function() {
	$('#project-one .project').css('color', '#333');
	let calss = $(this).hasClass('active');
	let name = $(this).parent().siblings('div.project').html();
	calss ? $(this).removeClass('active') : $(this).addClass('active')
	if (!calss && name === '冲刺科目') {
		$(this).parent().siblings('div.project').html($(this).html());
		headerList.project = $(this).html();
		return;
	}
	if (!calss) {
		$(this).parent().siblings('div.project').html(name + ',' + $(this).html());
		headerList.project = name + ',' + $(this).html();
		return;
	}
	let otherActive = $(this).siblings().hasClass('active');
	if (!otherActive) {
		$(this).parent().siblings('div.project').html('冲刺科目');
		$('#project-one .project').css('color', '#808080');
		headerList.project = '';
		return;
	}
	let list = name.replace($(this).html(), '').split(',');
	let new_data = '';
	let new_list = [];
	for (let i in list) {
		if (!list[i]) continue;
		new_list.push(list[i])
	}
	for (let j in new_list) {
		new_data += j === '0' ? new_list[j] : ',' + new_list[j]
	}
	$(this).parent().siblings('div.project').html(new_data);
	headerList.project = new_data;
})
// 冲刺科目的下拉点击
$('#pc-head-project li').click(function() {
	$('#pc-head-project .project').css('color', '#333');
	let calss = $(this).hasClass('active');
	let name = $(this).parent().siblings('div.project').html();
	calss ? $(this).removeClass('active') : $(this).addClass('active')
	if (!calss && name === '冲刺科目') {
		$(this).parent().siblings('div.project').html($(this).html());
		headerList.project = $(this).html();
		return;
	}
	if (!calss) {
		$(this).parent().siblings('div.project').html(name + ',' + $(this).html());
		headerList.project = name + ',' + $(this).html();
		return;
	}
	let otherActive = $(this).siblings().hasClass('active');
	if (!otherActive) {
		$(this).parent().siblings('div.project').html('冲刺科目');
		$('#pc-head-project .project').css('color', '#808080');
		headerList.project = '';
		return;
	}
	let list = name.replace($(this).html(), '').split(',');
	let new_data = '';
	let new_list = [];
	for (let i in list) {
		if (!list[i]) continue;
		new_list.push(list[i])
	}
	for (let j in new_list) {
		new_data += j === '0' ? new_list[j] : ',' + new_list[j]
	}
	$(this).parent().siblings('div.project').html(new_data);
	headerList.project = new_data;
})
// 意向班型的下拉点击
$('#class-none li').click(function() {
	headerList.class_name = $(this).html();
	$('#class-none .class-none').css('color', '#333');
	$(this).parent().siblings('div.class-none').html($(this).html());
})
// 意向班型的下拉点击
$('#pc-head-class li').click(function() {
	headerList.class_name = $(this).html();
	$('#pc-head-class .class-none').css('color', '#333');
	$(this).parent().siblings('div.class-none').html($(this).html());
})
// 冲刺科目的下拉点击
$('#project-two li').click(function() {
	// eightList.project = $(this).html();
	// $('#project-two .project').css('color', '#333');
	// $(this).parent().siblings('div.project').html($(this).html());
	
	$('#project-two .project').css('color', '#333');
	let calss = $(this).hasClass('active');
	let name = $(this).parent().siblings('div.project').html();
	calss ? $(this).removeClass('active') : $(this).addClass('active')
	if (!calss && name === '冲刺科目') {
		$(this).parent().siblings('div.project').html($(this).html());
		eightList.project = $(this).html();
		return;
	}
	if (!calss) {
		$(this).parent().siblings('div.project').html(name + ',' + $(this).html());
		eightList.project = name + ',' + $(this).html();
		return;
	}
	let otherActive = $(this).siblings().hasClass('active');
	if (!otherActive) {
		$(this).parent().siblings('div.project').html('冲刺科目');
		$('#project-two .project').css('color', '#999');
		eightList.project = '';
		return;
	}
	let list = name.replace($(this).html(), '').split(',');
	let new_data = '';
	let new_list = [];
	for (let i in list) {
		if (!list[i]) continue;
		new_list.push(list[i])
	}
	for (let j in new_list) {
		new_data += j === '0' ? new_list[j] : ',' + new_list[j]
	}
	$(this).parent().siblings('div.project').html(new_data);
	eightList.project = new_data;
})
// 冲刺科目的下拉点击
$('#pc-project-two li').click(function() {
	$('#pc-project-two .project').css('color', '#333');
	let calss = $(this).hasClass('active');
	let name = $(this).parent().siblings('div.project').html();
	calss ? $(this).removeClass('active') : $(this).addClass('active')
	if (!calss && name === '请选择薄弱科目') {
		$(this).parent().siblings('div.project').html($(this).html());
		eightList.project = $(this).html();
		return;
	}
	if (!calss) {
		$(this).parent().siblings('div.project').html(name + ',' + $(this).html());
		eightList.project = name + ',' + $(this).html();
		return;
	}
	let otherActive = $(this).siblings().hasClass('active');
	if (!otherActive) {
		$(this).parent().siblings('div.project').html('请选择薄弱科目');
		$('#pc-project-two .project').css('color', '#999');
		eightList.project = '';
		return;
	}
	let list = name.replace($(this).html(), '').split(',');
	let new_data = '';
	let new_list = [];
	for (let i in list) {
		if (!list[i]) continue;
		new_list.push(list[i])
	}
	for (let j in new_list) {
		new_data += j === '0' ? new_list[j] : ',' + new_list[j]
	}
	$(this).parent().siblings('div.project').html(new_data);
	eightList.project = new_data;
})
// 意向班型的下拉点击
$('#class-two li').click(function() {
	eightList.class_name = $(this).html();
	$('#class-two .class-none').css('color', '#333');
	$(this).parent().siblings('div.class-none').html($(this).html());
})
$('header div.btn').click(function() {
	headerList.name = isPc ? $('input#pc-head-name').val() : $('input#name-one').val();
	headerList.phone = isPc ? $('input#pc-head-phone').val() : $('input#phone-one').val();
	if (!headerList.name) return alert('请输入学生姓名');
	if (!headerList.project) return alert('请选择冲刺科目');
	if (!headerList.class_name) return alert('请选择意向班型');
	if (!headerList.phone) return alert('请输入家长电话');
	alert('获取中,请稍等');
})
$('.sesion-eight div.btn').click(function() {
	eightList.name = $('input#name-two').val();
	eightList.phone = $('input#phone-two').val();
	if (!eightList.name) return alert('请输入学生姓名');
	if (!eightList.project) return alert(isPc ? '请选择薄弱科目' : '请选择冲刺科目');
	if (!eightList.class_name && !isPc) return alert('请选择意向班型');
	if (!eightList.phone) return alert('请输入家长电话');
	alert('获取中,请稍等');
})
$('footer div.btn').click(function() {
	if (!$('input#student-name').val()) return alert('请输入学生姓名');
	if (!$('input#phone-three').val()) return alert('请输入电话号码');
	alert('提交成功,请稍等');
})

function dateCount() {
	// 获取现在的时间
	var date = new Date();
	var dateYear = date.getFullYear()
	// true 今年过完
	var isTure = date.valueOf() - new Date(dateYear, 06, 07, 08, 00, 00).valueOf() > 0 ? true : false;
	var riqi = isTure ? (dateYear + 1) + '-06-07 08:00:00' : dateYear + '-06-07 08:00:00';
	var until = new Date(isTure ? dateYear + 1 : dateYear, 06, 07, 08, 00, 00);
	// 计算时会发生隐式转换，调用valueOf()方法，转化成时间戳的形式
	var days = (until.valueOf() - date.valueOf()) / 1000 / 3600 / 24;
	// 下面都是简单的数学计算 
	var day = Math.floor(days);
	$('span#bennian').html(isTure ? dateYear + 1 : dateYear);
	var z = day >= 100 ? day.toString() : day > 10 ? '0' + day : '00' + day;
	var x = z.split('');
	$('span#time').html(x);

}
dateCount();
// 优秀教师点击
var teacher_list = [{
		name: '贺超',
		tab_list: [
			'独创方法',
			'经验丰富',
			'解题高手',
			'高效提分',
		],
		project: '政治',
		desc: '毕业于南开大学周恩来管理学院，获得高级政治老师资格证，从事高中政治教学5年，打破政治枯燥无味的教学模式，反对死记硬背，主张技巧性方法和大招的讲解，精准把握高考考点和命题趋势，擅长用鲜明案例讲解知识点和题型，便于学生掌握和理解。'
	},
	{
		name: '谢竺怡',
		tab_list: [
			'方法独特',
			'理性思维',
			'经验丰富',
			'课堂有料',
		],
		project: '历史',
		desc: '历史专业学士，专业功底扎实，持有高级中学教师资格证。深入研究高考考点考纲，熟悉全国卷高考规律，熟知不同类型学生的学习症结，善于解决各种学习问题，突破重难点。教学中注重学生的主体地位，启发式引导型授课，充分调动学生的主观能动性，给予思维结构而不局限于知识。课堂氛围活跃有趣，让学生快乐学习，轻松提分。'
	},
	{
		name: '邱雪',
		tab_list: [
			'方法独特',
			'掌握核心',
			'经验丰富',
			'精通高考',
		],
		project: '生物',
		desc: '邱老师，毕业于重点师范院校，从事生物教学多年，独创“生物背记技巧”，帮助学生夯实基础，注重学习思维能力培养，注重培养学生解题逻辑推断能力，倡导翻转课堂，学生才是课堂的主体。课堂幽默风趣，将生物与生活紧密联系，获得学生与家长高度认可。'
	},
	{
		name: '李媛媛',
		tab_list: [
			'方法独特',
			'条理清晰',
			'经验丰富',
			'亲和力强',
		],
		project: '英语',
		desc: '主讲初高中英语。从事中学英语教学6年，多届高中全日制经验。为人细心耐心，课堂氛围严肃活泼。教学特色：善于将语法逻辑化，帮助学生搭建成体系。考法分类，按学生类型针对性教学。经验总结：去标签化，每个孩子都能焕发潜力。'
	},
	{
		name: '田心怡',
		tab_list: [
			'幽默风趣',
			'经验丰富',
			'解题高手',
			'循序善诱',
		],
		project: '物理',
		desc: '9年初高中物理班课教学经验，深入研究中高考考纲考点。课堂风趣幽默、生动形象，通过 过关方式提高课堂效率，注重学习动力的激发和学习习惯的培养，帮助众多中高考学生取得骄人的成绩。教学理念：授之以渔不如授之以渔。'
	},
	{
		name: '杨陈',
		tab_list: [
			'熟悉考点',
			'经验丰富',
			'幽默风趣',
			'逻辑严谨',
		],
		project: '数学',
		desc: '9年高中教学经验，毕业于电子科技大学数学专业，从事数学教学多年，熟悉高考考点、考 纲和常考题型，课上重难点突出，逻辑严谨， 擅长把抽象复杂的数学问题简单化，让学生醍醐灌顶。历届高三学生都有明显提分，深受学生与家长的喜爱。'
	},
	{
		name: '李治君',
		tab_list: [
			'经验丰富',
			'思维培养',
			'解题高手',
			'幽默风趣',
		],
		project: '数学',
		desc: '李老师从事高中数学教学和教法研究多年。曾任教某重点中学，对高考考点把握准确！注重培养学生的解题思路，独创“问题-条件-关系”解题法，幽默的教学风格深受学生喜爱！一直坚信“没有教不好的学生，只有不会教的老师！”。'
	},
	{
		name: '方斯剑',
		tab_list: [
			'条理清晰',
			'思维培养',
			'方法独特',
			'幽默风趣',
		],
		project: '化学',
		desc: '方斯剑老师幽默风趣，让学生爱上课堂爱上化学；侧重知识体系的建立，给学生科学的记忆方法；脱离传统的题海，将大量练题总结出来的技巧和方法教授给学生；不止局限于学科专业知识，并对解题技巧考试技巧做出引导。'
	},
	{
		name: '邓秋平',
		tab_list: [
			'自创方法',
			'掌握重点',
			'经验丰富',
			'亲和力强',
		],
		project: '化学',
		desc: '硕士研究生学历，原市重点中学化学教师， 10年教龄，7年高三化学毕业班教学工作，深谙高考题型各类妙解方法，带领众多学生高考化学达到满分。对考纲研究透彻，对重难点了如指掌，自创了一套“秒杀”口诀，知识点讲解通俗易懂，快速准确。 '
	},
]
var teacher_html = '';
for (var i = 0; i < teacher_list.length; i++) {
	teacher_html +=
		`
	<div class="swiper-slide" name="${i}">
		<div class="back-img flex-box ${i === 0?'active': ''}" style="background-image: url(${isPc? 'pc/teacher/' + teacher_list[i].name + '/' + teacher_list[i].name : 'mobile/teacher/'+ teacher_list[i].name + '/' + teacher_list[i].name}.png)">
			<div class="left-box"></div>
			<div>
				<p class="name">${teacher_list[i].name}</p>
				<p>名师荟特级</p>
				<p>${teacher_list[i].project}</p>
			</div>
			<p class="back-img"></p>
		</div>
	</div>
	`;
}
$('#teacher-banner .swiper-wrapper').html(teacher_html);
showTeacherHtml(teacher_list[0]);
// 老师渲染函数
function showTeacherHtml(obj) {
	let choose_teacher_html = '';
	let teacher_img_html = '';
	choose_teacher_html =
		`
		<div class="right-box">
			<div class="nickname flex-box">
				${obj.name}<span>名师荟特级${obj.project}教师</span>
			</div>
			<p>${obj.desc}</p>
		</div>
	<div class="left-box" style="right: ${!isPc && obj.name === '邓秋平'? '.1rem': 0}">
		<img src="${isPc? 'pc': 'mobile'}/teacher/${obj.name}/${obj.name + '1'}.png">
	</div>
	`;
	teacher_img_html =
		`
	<img src="${isPc? 'pc': 'mobile'}/teacher/${obj.name}/1.jpg">
	<img src="${isPc? 'pc': 'mobile'}/teacher/${obj.name}/2.jpg">
	<img style="display:${isPc? 'inline-block': 'none'}" src="pc/teacher/${obj.name}/3.jpg">
	`;
	$('.choose-teacher').html(choose_teacher_html);
	$('.teacher-img').html(teacher_img_html);
}
var teacherBanner = new Swiper('#teacher-banner', {
	autoplay: 3000,
	// autoplay: false,
	loop: true,
	slidesPerView: isPc ? 3 : 4,
	// onlyExternal: true,
	spaceBetween: isPc ? 74 : 25,
	autoplayDisableOnInteraction: false,
	prevButton: '.teacher-banner-prev',
	nextButton: '.teacher-banner-next',
})
$('#teacher-banner').hover(function() {
	teacherBanner.stopAutoplay();
}, function() {
	teacherBanner.startAutoplay();
});
$('#teacher-banner .swiper-slide').click(function() {
	$(this).children('div').addClass('active');
	$(this).siblings().children('div').removeClass('active');
	showTeacherHtml(teacher_list[$(this).attr('name')])
})
// 备考流程点击事件
$('.btn-style > div > div').click(function() {
	var name = $(this).attr('name');
	$(this).addClass('active').siblings().removeClass('active');
	$(this).children('img').attr('src', 'pc/efficiency/点击按钮.png').parent().siblings().children('img').attr('src',
		'pc/efficiency/未点击按钮.png');
	$('.' + name).addClass('active').siblings().removeClass('active');
})
var showOne = true;
$(document).scroll(function() {
	var scroH = $(document).scrollTop(); //滚动高度
	if (scroH <= 1000) {
		showOne = false;
		$('footer').css('display', 'none');
		return;
	}
	if (showOne || !isPc) return;
	showOne = true;
	$('footer').css('display', 'block');
});

// 更改灵活班型的图片
$('img#sesion-four-1').attr('src', isPc ? 'pc/class/选择班型1-1.png' : 'mobile/class/a-1.png');
$('img#sesion-four-2').attr('src', isPc ? 'pc/class/选择班型2-1.png' : 'mobile/class/a-2.png');
$('img#sesion-four-3').attr('src', isPc ? 'pc/class/选择班型3-1.png' : 'mobile/class/b-1.png');
$('img#sesion-four-4').attr('src', isPc ? 'pc/class/选择班型4-1.png' : 'mobile/class/b-2.png');
$(document).bind("click", function(e) {
	var target = $(e.target);
	if (target.closest('#pc-head-project-menu').length == 1 || target.closest('#pc-head-project').length == 1 &&
		isProject) {
		$('#pc-head-project-menu').css('display', 'block')
		if (target.closest('#pc-head-project-menu').length == 1) return isProject = false;
	} else {
		$('#pc-head-project-menu').css('display', 'none')
		isProject = true;
	}

	if (target.closest('#project-one-menu').length == 1 || target.closest('#project-one').length == 1 &&
		isProject) {
		$('#project-one-menu').css('display', 'block')
		if (target.closest('#project-one-menu').length == 1) return isProject = false;
	} else {
		$('#project-one-menu').css('display', 'none')
		isProject = true;
	}

	if (target.closest('#pc-project-two-menu').length == 1 || target.closest('#pc-project-two').length == 1 &&
		isProject8) {
		$('#pc-project-two-menu').css('display', 'block')
		if (target.closest('#pc-project-two-menu').length == 1) return isProject8 = false;
	} else {
		$('#pc-project-two-menu').css('display', 'none')
		isProject8 = true;
	}
	if (target.closest('#project-two-menu').length == 1 || target.closest('#project-two').length == 1 &&
		isProject8) {
		$('#project-two-menu').css('display', 'block')
		if (target.closest('#project-two-menu').length == 1) return isProject8 = false;
	} else {
		$('#project-two-menu').css('display', 'none')
		isProject8 = true;
	}
})

function xunwenClick () {
	// let a = document.createElement('a');
	// a.href = 'https://tb.53kf.com/code/stat/10146118/1';
	// // a.target = '_blank';
	// a.click()
	 window.open ('https://tb.53kf.com/code/stat/10146118/1', '咨询', 'height=650, width=850, top=0, left=0, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=n o, status=no') 
}
$('#mobile-footer-choose-project > div').click(function() {
	if($(this).hasClass('active')) return;
	$(this).addClass('active').siblings().removeClass('active')
})
$('#mobile-head-choose-project > div').click(function() {
	if($(this).hasClass('active')) return;
	$(this).addClass('active').siblings().removeClass('active')
})
function playPhone() {
	if (isPc) return xunwenClick();
	let a = document.createElement('a');
	a.herf = 'tel:400-855-2262';
	a.click();
}
$('.video-play').click(function() {
	let video = document.getElementById('myVideo');
	video.controls = true;
	video.play()
})